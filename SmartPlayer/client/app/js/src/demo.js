$(document).ready(function () {

  //---------------------------------------
  //#region Demo Button Clicks

   $('.demoBtn').click(function () {
    $(this).toggleClass('checked')
  });

   $('.demoBtn2').click(function () {
    // Tip: try other integers [1-67] at PageTransitions.goToPage function
    // and see different animations on changing pages
    PageTransitions.goToPage(1, 'page2');

	
  }); 
  
  $('.demoBtn7').click(function () {
    // Tip: try other integers [1-67] at PageTransitions.goToPage function
    // and see different animations on changing pages
    PageTransitions.goToPage(1, 'page4');
  }); 
  
   $('#go_music').click(function () {
    // Tip: try other integers [1-67] at PageTransitions.goToPage function
    // and see different animations on changing pages
    PageTransitions.goToPage(1, 'page3');
  });
  
  $('#go_video').click(function () {
    // Tip: try other integers [1-67] at PageTransitions.goToPage function
    // and see different animations on changing pages
    PageTransitions.goToPage(1, 'page1');
  });
  

  $('.homebutton').click(function () {
    // Tip: try other integers [1-67] at PageTransitions.goToPage function
    // and see different animations on changing pages
    PageTransitions.goToPage(1, 'page1');
  });
  
  $('.demoBtn5').click(function () {
    // Tip: try other integers [1-67] at PageTransitions.goToPage function
    // and see different animations on changing pages
    PageTransitions.goToPage(1, 'page5');
  });
  
  
  
  $('#mute_b').click(function () {
	var my_video;
	vid = document.getElementById("my_video");
	decrease_volume(vid);
  });
  
  
  $('#unmute_b').click(function () {
	var my_video;
	vid = document.getElementById("my_video");
	
	increase_volume(vid);
  });
  
  $('#mute_b2').click(function () {
	var my_video;
	vid = document.getElementById("my_video2");
	
	decrease_volume(vid);
  });
  
  
  $('#unmute_b2').click(function () {
	var my_video;
	vid = document.getElementById("my_video2");
	
	increase_volume(vid);
  });
  
   $("#myBtn2").click(function(){
        $("#myCarousel4").carousel("next");
    }); 
  
	$("#myBtn3").click(function(){
        $("#myCarousel4").carousel("prev");
    });
	$("#myBtn21").click(function(){
        $("#myCarousel10").carousel("next");
    }); 
  
	$("#myBtn31").click(function(){
        $("#myCarousel10").carousel("prev");
    });
  	
	$(".next_b").click(function(){
        PageTransitions.goToPage(1, 'page5');
    });
	
	
	$("#next_b").click(function(){
        PageTransitions.goToPage(1, 'page5');
    });
	
	$('.play_m').click(function () {
    var my_video;
	
	vid = document.getElementById("my_video");
	playbtn = document.getElementById("playpausebtn");
	playPause(vid, playbtn);
  });
	
	$(".prev_b").click(function(){
        PageTransitions.goToPage(1, 'page5');
    });

	$("#prev_b").click(function(){
        PageTransitions.goToPage(1, 'page5');
    });
	
	$(".next_b1").click(function(){
        PageTransitions.goToPage(1, 'page2');
    });

	$("#next_b1").click(function(){
        PageTransitions.goToPage(1, 'page2');
    });
	
	$(".prev_b1").click(function(){
        PageTransitions.goToPage(1, 'page2');
    });
	
	$("#prev_b1").click(function(){
        PageTransitions.goToPage(1, 'page2');
    });
	
	$('.play_m1').click(function () {
    var my_video;
	
	vid = document.getElementById("my_video2");
	playbtn = document.getElementById("playpausebtn2");
	playPause(vid, playbtn);
  });
  
  $('.play_m2').click(function () {
    var my_video;
	vid = document.getElementById("audio1");
	playbtn = document.getElementById("p3");
	playPause(vid, playbtn);
  });
  
   
  $('#mute_b3').click(function () {
	var my_video;
	vid = document.getElementById("audio1");	
	decrease_volume(vid);
  });
  
  
  
  
  $('#unmute_b3').click(function () {
	var my_video;
	vid = document.getElementById("audio1");
	
	increase_volume(vid);
  });
  
   $("#myBtn8").click(function(){
        $("#myCarousel9").carousel("next");
    }); 
  
	$("#myBtn9").click(function(){
        $("#myCarousel9").carousel("prev");
    });
  

});



function playPause(vid, playbtn){
	if(vid.paused){
		vid.play();
		playbtn.src="app/images/pause.png";
	} else {
		vid.pause();
		playbtn.src="app/images/play.png";
	}
}

function play(vid){
	if(vid.paused){
		vid.play();
	} 
}


function vidmute(vid){
	vid.muted = true;
}

function vidunmute(vid){
	vid.muted = false;
}

function stop(vid){
	if(vid.play){
		vid.pause();
	}
}

function increase_volume(vid,){
	if (vid.volume < 1)vid.volume = vid.volume+0.25;
}

function decrease_volume(vid){

	if (vid.volume > 0.25)vid.volume = vid.volume-0.25;
}


