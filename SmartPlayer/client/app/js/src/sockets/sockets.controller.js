//*******************************************
//  SOCKETS CONTROLLER
//*******************************************
var Sockets = (function () {
  var socketConnection = null;

  /**
   * Connect to socket server
   */
  function Connect() {
    socketConnection = new WebSocket(GlobalConfig.Connections.Sockets);

    socketConnection.onopen = function () {
      console.info("Sockets connected!");
    };

    socketConnection.onerror = function (error) {
      console.info('Sockets Error: ' + error);
    };

    socketConnection.onmessage = function (e) {
		
		
      var msg = jQuery.parseJSON(e.data);
	
      var msgType = msg.channel;
	  var msg_msg = msg.message;
	  
	  if (msgType == 'fs') {
       PageTransitions.goToPage(1, 'page2');
      }

	  
      if (msgType == 'kinect/movement') {
        // move cursor
        Gestures.Manager(`hand:show:${msg.message.handType.toLowerCase()}`);
        Cursor.Move(msg.message.y, msg.message.x);
      } else if (msgType == 'sensors/touch') {

		console.log("im innnnnnnnnn");
		if(document.getElementById("activepage").innerHTML=="page5"){
			var playpause = document.getElementById("playpausebtn2");
			playpause.click();
		}
		if(document.getElementById("activepage").innerHTML=="page2"){
			var playpause = document.getElementById("playpausebtn");
			playpause.click();
		}
		if(document.getElementById("activepage").innerHTML=="page4"){
			var playpause = document.getElementById("p3");
			playpause.click();
		}
		
      } else if (msgType == "kinect/gesture") {

		var kk=msg.message.gestureType;
		var bc;
		var bc1,bc2;
		
		if(kk=='SwipeLeft'){
			console.log("leftttt");
			bc = document.getElementById("myBtn3");
			bc.click();
			bc1 = document.getElementById("myBtn31");
			bc1.click();
			bc2 = document.getElementById("myBtn9");
			bc2.click();
			
		}else if(kk=='SwipeRight'){
			console.log("RIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIGHT");
			bc = document.getElementById("myBtn2");
			bc.click();
			bc1 = document.getElementById("myBtn21");
			bc1.click();
			bc2 = document.getElementById("myBtn8");
			bc2.click();
		}else if(kk=='SwipeDownRight'){
			console.log("SwipeDownRight");
			bc = document.getElementById("mute_b");
			bc.click();
			bc1 = document.getElementById("mute_b2");
			bc1.click();
			bc2 = document.getElementById("mute_b3");
			bc2.click();
		}else if(kk=='SwipeUpRight'){
			console.log("SwipeUpRight");
			bc = document.getElementById("unmute_b");
			bc.click();
			bc1 = document.getElementById("unmute_b2");
			bc1.click();
			bc2 = document.getElementById("unmute_b3");
			bc2.click();
		}
		
      } else if (msgType == 'sensors/touchslider') {
		  // console.log("----------------touch slider ---------------------------");
		   var volumex = msg.message.actualValue;
		   volumex=volumex/1000;
		    var ser = msg.message.serialNumber;
			var bcv = document.getElementById("vval");
			 $('#vval').click(function () {
				var vid = document.getElementById("my_video2");
					vid.volume = volumex;
			});
			bcv.click();
			
			var bcv2 = document.getElementById("vval2");
			 $('#vval2').click(function () {
				var vid2 = document.getElementById("my_video");
					vid2.volume = volumex;
			});
			bcv2.click();
			
			var bcv3 = document.getElementById("vval3");
			 $('#vval3').click(function () {
				var vid3 = document.getElementById("audio1");
					vid3.volume = volumex;
			});
			bcv3.click();
      } 
	  else if (msgType == 'kinect/speech') {
		  console.log(msg.message.speechValue);
		  
		  if(document.getElementById("activepage").innerHTML=="page5"){
			//var playpause = document.getElementById("playpausebtn2");
			//playpause.click();
			if ((msg.message.speechValue) == 'PLAY'){
			var playpause = document.getElementById("playpausebtn2");
			playpause.click();
		  }
		  else if ((msg.message.speechValue) == 'VOLUMEUP'){
			var volume_up = document.getElementById("unmute_b2");
			volume_up.click();
		  }
		  else if ((msg.message.speechValue) == 'VOLUMEDOWN'){
			var volume_down = document.getElementById("mute_b2");
			volume_down.click();
		  }
		 else if ((msg.message.speechValue) == 'NEXT'){
			var next_video = document.getElementById("next_b1");
			next_video.click();
		  }
		  else if ((msg.message.speechValue) == 'PREV'){
			var prev_video = document.getElementById("prev_b1");
			prev_video.click();
		  }
		}
		else if(document.getElementById("activepage").innerHTML=="page2"){
			//var playpause = document.getElementById("playpausebtn2");
			//playpause.click();
			if ((msg.message.speechValue) == 'PLAY'){
			var playpause = document.getElementById("playpausebtn");
			playpause.click();
		  }
		  else if ((msg.message.speechValue) == 'VOLUMEUP'){
			var volume_up = document.getElementById("unmute_b");
			volume_up.click();
		  }
		  else if ((msg.message.speechValue) == 'VOLUMEDOWN'){
			var volume_down = document.getElementById("mute_b");
			volume_down.click();
		  }
		 else if ((msg.message.speechValue) == 'NEXT'){
			var next_video = document.getElementById("next_b");
			next_video.click();
		  }
		  else if ((msg.message.speechValue) == 'PREV'){
			var next_video = document.getElementById("next_b");
			next_video.click();
		  }
		}
		else if(document.getElementById("activepage").innerHTML=="page4"){
			if ((msg.message.speechValue) == 'PLAY'){
			var playpause = document.getElementById("p3");
			playpause.click();
		  }
		  else if ((msg.message.speechValue) == 'VOLUMEUP'){
			var volume_up = document.getElementById("unmute_b3");
			volume_up.click();
		  }
		  else if ((msg.message.speechValue) == 'VOLUMEDOWN'){
			var volume_down = document.getElementById("mute_b3");
			volume_down.click();
		  }
		}  
	  }
    };
  }

  /**
   * Disconnect from socket server
   */
  function Disconnect() {
    socketConnection.disconnect();
    console.info('Sockets disconnected!');
  }

  /**
   * Send message to socket server
   * 
   * @param {string} type - Type of message
   * @param {any} msg - Object to send as message  
   */
  function SendMessage(type, msg) {  
	 
	socketConnection.send(JSON.stringify({
		channel: type,
        message: msg,
    }));
	
  }

  return {
    Connect: Connect,
    Disconnect: Disconnect,
    SendMessage: SendMessage
  };
})();