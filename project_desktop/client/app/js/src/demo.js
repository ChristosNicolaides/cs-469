$(document).ready(function () {

  //---------------------------------------
  //#region Demo Button Clicks

  $('.demoBtn').click(function () {
    $(this).toggleClass('checked')
  });

  $('.demoBtn2').click(function () {
    // Tip: try other integers [1-67] at PageTransitions.goToPage function
    // and see different animations on changing pages
    PageTransitions.goToPage(1, 'page2');
  });

  $('.demoBtn3').click(function () {
    // Tip: try other integers [1-67] at PageTransitions.goToPage function
    // and see different animations on changing pages
    PageTransitions.goToPage(2, 'page1');
  });

  //#endregion
  //---------------------------------------

});


function stop(vid){
	if(vid.play){
		vid.pause();
	}
}

function play(vid){
	if(vid.paused){
		vid.play();
	} 
}

function playPause(vid, playbtn){
	//var playbtn = document.getElementById("playpausebtn");
	if(vid.paused){
		vid.play();
		playbtn.src="app/images/pause.png";
	} else {
		vid.pause();
		playbtn.src="app/images/play.png";
	}
}
