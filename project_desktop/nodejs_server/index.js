
const WebSocket = require('ws');

const UI_SERVER_WEBSOCKET_PORT = 6556;


//create new websocket Server for UI clients
const wsServer = new WebSocket.Server({ port: UI_SERVER_WEBSOCKET_PORT });

//an array that stores the connected clients
var clients = [];

wsServer.on('connection', function connection(ws) {

    //new client connected - add client to list
    clients.push(ws);

    //callback that will be called whenever the client sends a message to the server
    ws.on('message', function incoming(message) {
        console.log(`received message from client: ` + message);
    });

    ws.on('close', function close() {
        clients.splice(clients.indexOf(ws), 1);
    });

});


/* ----------------------------------------------------- */


const webSocketSensorServer = require('./sensorsWebSocketServer');

var sensorsServer = webSocketSensorServer.startServer((channel, message) => {
    console.log(`channel: ${channel} , message: ${JSON.stringify(message)}`);

    //business logic
    //handle events, choose which client to send what

	//console.log("sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
	
    clients.forEach(client => {
        client.send(JSON.stringify({
            channel: channel,
            message: message,
        }));
    });

});






